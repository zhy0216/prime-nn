

PROJECT_ROOT = "./"
FEATURE_NAME = "nature-feature"

IMAGE_TEST = PROJECT_ROOT + "images/%s/test/"%FEATURE_NAME
LISTFILE_TEST = PROJECT_ROOT + "lenet-config/%s/listfile-test.txt"%FEATURE_NAME
IMAGE_TEST_DATA_PATH = PROJECT_ROOT + "data/%s/test"%FEATURE_NAME

IMAGE_TRAIN = PROJECT_ROOT + "images/%s/train/"%FEATURE_NAME
LISTFILE_TRAIN = PROJECT_ROOT + "lenet-config/%s/listfile-train.txt"%FEATURE_NAME
IMAGE_TRAIN_DATA_PATH = PROJECT_ROOT + "data/%s/train"%FEATURE_NAME

CAFEE_ROOT = "/home/yang/workspace/caffe/"
CONVERT_IMAGESET = CAFEE_ROOT + "build/tools/convert_imageset"

''' used in python binding '''
LENET_PROTOTXT = PROJECT_ROOT + "lenet-config/nature-feature/python_test.prototxt"

WEIGHTS = PROJECT_ROOT + "lenet-config/snap/_iter_15000.caffemodel"
# WEIGHTS = PROJECT_ROOT + "lenet/sym10_iter_10000.caffemodel"
LABEL_DICT = PROJECT_ROOT + "lenet-config/nature-feature/label-dict.json"


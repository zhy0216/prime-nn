import random
import os

from eulerlib import primes, is_prime
from PIL import Image
import numpy as np

## https://bitbucket.org/transmogrifier/eulerlib

class FeatureGen(object):
    width = height = 7
    name = "setyournamehere"
    limit = 1*10**6
    test_rate = 0.1

    def __init__(self):
        # self.prime_list = primes(self.limit)
        self.test_path = "images/" + self.name + "/test/"
        self.train_path = "images/" + self.name + "/train/"
        self.test_path_prime = self.test_path + "prime/"
        self.test_path_composite = self.test_path + "composite/"
        self.train_path_prime = self.train_path + "prime/"
        self.train_path_composite = self.train_path + "composite/"
        for path in [self.test_path_prime, self.test_path_composite, 
                    self.train_path_prime, self.train_path_composite]:
            if not os.path.exists(path):
                os.makedirs(path)

    def is_test_case(self, number=0):
        return random.random() < self.test_rate
        # return number > 10**6


    def save_data(self, number, filename):
        data = map(int, "{0:049b}".format(number)[::-1])
        # print data

        npdata = np.array(data).reshape(self.height, self.width)
        im = Image.fromarray(np.uint8(npdata*255))
        im.save(filename)
        # print list(im.getdata())

    def start(self):
        for i in range(2, self.limit):
            if i % 1000 == 0:
                print i
            extension = "prime/" if self.is_prime(i) else "composite/"
            if self.is_test_case(number=i):
                self.save_data(i, "%s%s%s.png"%(self.test_path, extension, i))
            else:
                self.save_data(i, "%s%s%s.png"%(self.train_path, extension, i))

    def is_prime(self, number):
        return is_prime(number)

class NatureFeatureGen(FeatureGen):
    name = "nature-feature"


if __name__ == "__main__":
    nature_feature_gen = NatureFeatureGen()
    nature_feature_gen.start()




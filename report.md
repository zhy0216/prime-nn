Yang Zhang (zhang505)  
B555  
Final Project: Can neural network predict prime number?

----------------------------

Motivation: 
The idea why I want to try to predict prime number by neural network is from Miller–Rabin primality test. The algorithm will tell whether a number is a prime number or not with probability. Since machine learning is all about probability, so probably I can solve this problem by using some machine learning method. I feel neural network may be the right tool for this problem. 

Approach: 
Instead of develop my own neural network, I use LeNet directly and the software I use is caffe. The only problem becomes how to make the right model to fit the LeNet. 

Data Preprocessing: 
I use python to deal with data. The code can be reach through the link below. I translate every number to binary number with left padding to 49 digits, and then inverse. For example, this number 46 will become [0, 1, 1, 1, 0, 1, 0, 0, .... 0].
And then I put this data into a 7x7 array and make is to a grey scale image. Then scale this image to 28x28. After that, the LeNet can be used to train this model.

Result:
    * Test 1 (base test):
        From number 1 to number 100,000, choose 10% data as test data. 
        accuracy = 0.903186
        baseline = 1 - # of prime / total number =  1-9592./(10^5) = 0.904

        Explain: The result is relative bad. I guess it is because data size is not big enough, so the neural network did not perform well. 

    * Test 2 (increase data size):
        From number 1 to number 1,000,000, choose 10% data as test data. 
        accuracy = 0.913059
        baseline = 0.921502

        Explain: The performence is also not good. I believe the problem may not be solve by neural network, but I want to try more test data to see how bad the accuracy will be.

    * Test 3 (increase test data size):
        I choose number 1 to number 1,000,000 as train data, choose number 1,000,000, to number 2,000,000 as test data.
        accuracy = 0.907106
        baseline = 0.925534

        Explain: The accuracy is below the baseline but not dramatically low. The reason is because there lots of number are not prime number. When the neural network were feed lots of composite number data, then it have high priority to predict the number is prime number.

    * Test 4 (increase data size):
        From number 1 to number 2,000,000, choose 10% data as test data.
        accuracy = 
        baseline = 0.9255335
        Explain

Try another feature?:
I did not try any other possible feature. At first, I thought about to use some formula to generate some decimal number as a feature, but I just realized decimal number were not meaturable. Therefore, to use decimal number as a feature is not reasonable. I also try to find some formula, which turns out none of them is useful.

Why failed:
collect some infomation


  






Thanks:
Thanks to my friend Yisu Peng who pointed out that when I do caffe test, I set the batch size too small so that the test result become unreasonable good. I even felt too exciting to discuss with Prof. Raphael. I have to say sorry to professor for my little mistake.


----------------------------
Resource:
* [LeCun et al., 1998a] Y. LeCun, L. Bottou, Y. Bengio, and P. Haffner. "Gradient-based learning applied to document recognition." Proceedings of the IEEE, 86(11):2278-2324, November 1998.
* @article{jia2014caffe,
  Author = {Jia, Yangqing and Shelhamer, Evan and Donahue, Jeff and Karayev, Sergey and Long, Jonathan and Girshick, Ross and Guadarrama, Sergio and Darrell, Trevor},
  Journal = {arXiv preprint arXiv:1408.5093},
  Title = {Caffe: Convolutional Architecture for Fast Feature Embedding},
  Year = {2014}
}
* http://primesieve.org/
* https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
* https://en.wikipedia.org/wiki/Formula_for_primes
* my code: https://bitbucket.org/zhy0216/prime-nn



